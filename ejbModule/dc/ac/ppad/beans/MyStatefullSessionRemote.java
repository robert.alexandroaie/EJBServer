package dc.ac.ppad.beans;

import javax.ejb.Remote;

@Remote
public interface MyStatefullSessionRemote {
	
	  public int getAndIncrement();
}
