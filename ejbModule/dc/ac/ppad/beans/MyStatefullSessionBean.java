package dc.ac.ppad.beans;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

/**
 * Session Bean implementation class MyStatefullSessionBean
 */
@Stateful
@LocalBean
public class MyStatefullSessionBean implements MyStatefullSessionRemote {

	private int cnt;

	public MyStatefullSessionBean() {

		System.out.println("Constructor called: " + MyStatefullSessionBean.class.getName());
	}

	public int getAndIncrement() {
		
		System.out.println("getAndIncrement called...");
		return cnt++;
	}

}
