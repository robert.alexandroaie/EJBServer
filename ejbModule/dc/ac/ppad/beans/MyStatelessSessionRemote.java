package dc.ac.ppad.beans;

import javax.ejb.Remote;

@Remote
public interface MyStatelessSessionRemote {

	public String runEcho(String string);
}
